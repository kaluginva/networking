//
//  MainScreenTableViewCell.swift
//  Netwoking
//
//  Created by Vladislav Kalugin on 07.01.2021.
//

import UIKit

class MainScreenTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fullName: UILabel!
    
    private var task: URLSessionDataTask?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        task?.cancel()
        avatarView.image = nil
       
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
        
    func configure() {
        avatarView.layer.cornerRadius = avatarView.frame.height / 2
    }
}
