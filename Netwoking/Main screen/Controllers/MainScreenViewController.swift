//
//  ViewController.swift
//  Netwoking
//
//  Created by Tatyana Anikina on 07.01.2021.
//

import UIKit
import Foundation

class MainScreenViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicate: UIActivityIndicatorView!
    
    private var repositoryDescriptionFirst: String?
    private var userName: String?
    private var avatarImage: String?
    private var repositoryName: String?
    private var boolStatus: Bool?
    private var urlData: String?
    private var urlFollowers: String?
    
    let imageLoader = ImageCacheLoader()
    
    
    var arrayRepositories: [Repositories] = []
    
    // Для пагинации
    var exileURL: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Repositories"
        
        tableView.register(UINib(nibName: "MainScreenTableViewCell", bundle: nil), forCellReuseIdentifier: "MainScreenTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        fetchData(urlString: "https://api.github.com/repositories")
        
        createFetchRefresh()
        
    }
    
    
    func createFetchRefresh() {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_ :)), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Обновление таблицы")

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
       
    }
    
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        self.tableView.reloadData()
        refreshControl.endRefreshing()
       
    }
    
    
    func fetchData(urlString: String) {
        
        activityIndicate.startAnimating()
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { [self] (data, responce, error) in
            guard let data = data else { return }
            
            do {
                self.arrayRepositories.append(contentsOf: try JSONDecoder().decode([Repositories].self, from: data))
                
                if let responce = responce as? HTTPURLResponse {
                    if let linkValue = responce.allHeaderFields["Link"] as? String {
                        let links = linkValue.components(separatedBy: ",")

                        var dictionary: [String: String] = [:]
                        links.forEach({
                            let components = $0.components(separatedBy:"; ")
                            let cleanPath = components[0].trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
                            dictionary[components[1]] = cleanPath
                            
                        })
                        
                        if let nextPagePath = dictionary["rel=\"next\""] {
                           exileURL = nextPagePath
                            
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
             } catch {
                print(error.localizedDescription)
            }
            
        
        }.resume()
    }
    
    
    private func configureCell(cell: MainScreenTableViewCell, for indexPath: IndexPath) {
        let repositories = arrayRepositories[indexPath.row]
        
        cell.nameLabel.text = repositories.name ?? ""
        cell.fullName.text = repositories.full_name ?? ""
        
        let lastRow = indexPath.row
        
        if lastRow == arrayRepositories.count - 1 {
            fetchData(urlString: exileURL ?? "")
        }
    }
    
    //Чтобы иметь доступ к другому контроллеру
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let webViewController = segue.destination as! DescriptionScreenViewController
        webViewController.repositoryNameTitle = repositoryName
        webViewController.userName = userName
        webViewController.repositoryDescription = repositoryDescriptionFirst
        webViewController.dateLabelString = urlData
        webViewController.followersLabelString = urlFollowers
       
        
        guard let imageUrl = URL(string: avatarImage ?? "") else { return }
        guard let imageData = try? Data(contentsOf: imageUrl) else { return }
        webViewController.avatarImage = UIImage(data: imageData)
        
        webViewController.status = boolStatus!
        
    }
}


extension MainScreenViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRepositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainScreenTableViewCell") as! MainScreenTableViewCell
        let repositories = arrayRepositories[indexPath.row]
        configureCell(cell: cell, for: indexPath)
        
        imageLoader.obtainImageWithPath(imagePath: repositories.owner?.avatar_url ?? "") { (image) in
            if let cell = tableView.cellForRow(at: indexPath) as? MainScreenTableViewCell {
                cell.avatarView.image = image
            }
        }
        
        return cell
    }
    
    // Переход по нажатию ячейки
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let repository = arrayRepositories[indexPath.row]
        
        userName = repository.owner?.login
        repositoryDescriptionFirst = repository.description
        avatarImage = repository.owner?.avatar_url
        boolStatus = repository.private
        repositoryName = repository.name
        
        
        // Получение новой url даты создания
        urlData = repository.url
        
        // Получение новой url для отображения фолловеров
        urlFollowers = repository.owner?.followers_url
    
        performSegue(withIdentifier: "Description", sender: self)
        
    }
}

//Менеджер для нормальной загрузки ячеек
typealias ImageCacheLoaderCompletionHandler = ((UIImage) -> ())

class ImageCacheLoader {
    
    var task: URLSessionDownloadTask!
    var session: URLSession!
    var cache: NSCache<NSString, UIImage>!
    
    init() {
        session = URLSession.shared
        task = URLSessionDownloadTask()
        self.cache = NSCache()
    }
    
    func obtainImageWithPath(imagePath: String, completionHandler: @escaping ImageCacheLoaderCompletionHandler) {
        if let image = self.cache.object(forKey: imagePath as NSString) {
            DispatchQueue.main.async {
                completionHandler(image)
            }
        } else {

            let url: URL! = URL(string: imagePath)
            task = session.downloadTask(with: url, completionHandler: { (location, response, error) in
                if let data = try? Data(contentsOf: url) {
                    let img: UIImage! = UIImage(data: data)
                    self.cache.setObject(img, forKey: imagePath as NSString)
                    DispatchQueue.main.async {
                        completionHandler(img)
                    }
                }
            })
            task.resume()
        }
    }
}
