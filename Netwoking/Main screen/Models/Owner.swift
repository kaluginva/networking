//
//  Owner.swift
//  Netwoking
//
//  Created by Vladislav Kalugin on 08.01.2021.
//

import Foundation

struct Owner: Decodable {
    
    var avatar_url: String?
    var login: String?
    var followers_url: String?
}
