//
//  Repositories.swift
//  Netwoking
//
//  Created by Vladislav Kalugin on 07.01.2021.
//

import Foundation

struct Repositories: Decodable {
    
    var full_name: String?
    var name: String?
    var description: String?
    var owner: Owner?
    var `private`: Bool?
    var url: String?
    var created_at: String?
//    var login: [String]?
    
}
