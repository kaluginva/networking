//
//  DescriptionScreenViewController.swift
//  Netwoking
//
//  Created by Vladislav Kalugin on 07.01.2021.
//

import UIKit

class DescriptionScreenViewController: UIViewController {

    @IBOutlet weak var avatarMiniImage: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var localStatus: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameRepositLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var activityIndicate: UIActivityIndicatorView!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var activityIndicatorFollowers: UIActivityIndicatorView!
    
    
    var repositoryNameTitle: String?
    var userName: String?
    var repositoryDescription: String?
    var avatarImage: UIImage?
    var status: Bool = false
    var dateLabelString: String?
    var urlData: String = ""
    var followersLabelString: String?
    var urlFollowers: String = ""

    var сreateData: Repositories?
    var loginFollowers: [Followers] = []
    
    let refreshControll = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicate.isHidden = true
        activityIndicate.hidesWhenStopped = true
        
        getDescriptionRepositories()
        configure()

        layoutConstraintForLabel()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        getDescriptionRepositories()
        fetchData(urlString: urlData)
    }
    
    
    func configure() {
        avatarMiniImage.layer.cornerRadius = avatarMiniImage.frame.height / 2
    }
    
    
    func getDescriptionRepositories() {
        
        loginLabel.text = userName
        
        descriptionLabel.text = repositoryDescription
        descriptionLabel.setNeedsDisplay()
        
        nameRepositLabel.text = repositoryNameTitle
        
        avatarMiniImage.image = avatarImage
        localStatus.text = "Рrivacy: \(status)"
        
        urlData = dateLabelString!
        urlFollowers = followersLabelString!

    }


    func fetchData(urlString: String) {
        
        activityIndicate.isHidden = false
        activityIndicate.startAnimating()
    
                
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { [self] (data, responce, error) in
            guard let data = data else { return }
            
            do {
                self.сreateData =  try JSONDecoder().decode(Repositories.self, from: data)
                
                DispatchQueue.main.async {
                    self.activityIndicate.stopAnimating()
                    dateLabel.text = сreateData?.created_at?.convertDate()
                }
                
            } catch {
                print(error.localizedDescription)
            }
            
        
        }.resume()
    }
    
    
    func fetchDataFollowers(urlFollowers: String) {
        
        guard let url = URL(string: urlFollowers) else { return }
        URLSession.shared.dataTask(with: url) { [self] (data, responce, error) in
            guard let data = data else { return }

            do {
                self.loginFollowers.append(contentsOf: try JSONDecoder().decode([Followers].self, from: data))

                DispatchQueue.main.async {
                    followersCount.text = String(loginFollowers.count)


                }
             } catch {
                print(error.localizedDescription)
            }
            
        
        }.resume()
    }
    
    func layoutConstraintForLabel() {
        
        descriptionLabel.numberOfLines = 0
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.lineBreakMode = .byWordWrapping
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: localStatus.layoutMarginsGuide.topAnchor, constant: 50),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
        ])
        
    }
}

