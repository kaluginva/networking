//
//  String + Extension.swift
//  Netwoking
//
//  Created by Vladislav Kalugin on 13.01.2021.
//

import Foundation

extension String {

        func convertDate() -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

            if let dateInLocal = dateFormatter.date(from: self) {
                dateFormatter.dateFormat = "dd-MM-yyyy"
                return dateFormatter.string(from: dateInLocal)
            }
            return "NA"
        }

    }

